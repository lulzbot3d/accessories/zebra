X = [0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,];
Y = [0, 0, 0, 0, 0, 67.5, 67.5, 67.5, 67.5, 67.5, 135, 135, 135, 135, 135, 202.5, 202.5, 202.5, 202.5, 202.5, 270, 270, 270, 270, 270];
Z = [0, .098, .133, .086, -.043, .281, .269, .235, .147, .041, .384, .321, .270, .198, .110, .215, .218, .216, .175, .105, -.095, .014, .077, .076, -.007 ];
xr = reshape(X, 5, 5);
yr = reshape(Y, 5, 5);
zr = reshape(Z, 5, 5);
surf(xr, yr, zr)
zlim([-1 1])
xlabel('x position (mm)')
ylabel('y position (mm)')
zlabel('height (mm)')
title("Felt 2 Part  Bed, Wires Smashed, 20C");
