X = [0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,];
Y = [0, 0, 0, 0, 0, 67.5, 67.5, 67.5, 67.5, 67.5, 135, 135, 135, 135, 135, 202.5, 202.5, 202.5, 202.5, 202.5, 270, 270, 270, 270, 270];
Z = [0, .106, .154, .073, -.080, .175, .247, .284, .212, .087, .198, .283, .326, .283, .162, .084, .193, .263, .237, .129, -.075, .047, .140, .1, -.051];
xr = reshape(X, 5, 5);
yr = reshape(Y, 5, 5);
zr = reshape(Z, 5, 5);

e = interp2(xr)
f = interp2(yr)
g = interp2(zr)

E = interp2(e)
F = interp2(f)
G = interp2(g)

surf(E, F, G)
zlim([-1 1])
xlabel('x position (mm)')
ylabel('y position (mm)')
zlabel('height (mm)')
title("Felt 2pt Bed 2, 130C");
