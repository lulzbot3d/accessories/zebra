X = [0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,];
Y = [0, 0, 0, 0, 0, 67.5, 67.5, 67.5, 67.5, 67.5, 135, 135, 135, 135, 135, 202.5, 202.5, 202.5, 202.5, 202.5, 270, 270, 270, 270, 270];
Z = [0, -.024, -.043, -.051, -.036, -.019, -.032, -.049, -.066, -.072, -.060, -.045, -.046, -.041, -.038, -.105, -.077, -.051, -.015, .008, -.110, -.096, -.075, -.033, .003 ];
xr = reshape(X, 5, 5);
yr = reshape(Y, 5, 5);
zr = reshape(Z, 5, 5);
surf(xr, yr, zr)
zlim([-1 1])
xlabel('x position (mm)')
ylabel('y position (mm)')
zlabel('height (mm)')
title("Standard Bed, Wires Not Smashed, 20C");
