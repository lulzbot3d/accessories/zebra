X = [0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270, 0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,0, 67.5, 135, 202.5, 270,];
Y = [0, 0, 0, 0, 0, 67.5, 67.5, 67.5, 67.5, 67.5, 135, 135, 135, 135, 135, 202.5, 202.5, 202.5, 202.5, 202.5, 270, 270, 270, 270, 270];
Z = [0, -.005, -.022, -.043, -.053, .212, .118, .031, -.035, -.078, .30, .161, .052, -.013, -.057, .123, .052, -.008, -.035, -.061, -.132, -.143, -.143, -.143, -.140];
xr = reshape(X, 5, 5);
yr = reshape(Y, 5, 5);
zr = reshape(Z, 5, 5);
surf(xr, yr, zr)
zlim([-1 1])
xlabel('x position (mm)')
ylabel('y position (mm)')
zlabel('height (mm)')
title("Standard Bed, Wires Smashed, 20C");
