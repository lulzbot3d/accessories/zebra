;test with digital indicator on heat bed to check for flattness in 9 points

G26                          ; clear potential 'probe fail' condition
G21                          ; set units to Millimetres
M107                         ; disable fans
G90                          ; absolute positioning
M82                          ; set extruder to absolute mode
G28 XY                       ; home X and Y
G1 X-19 Y258 F1000           ; move to safe homing position
G28 Z                        ; home Z
G1 Z10                       ; raise extruder
G1 X-9 Y-9 F5000                  ; move above probe
M204 S100                    ; set accel for probing
G29
M204 S500                    ; set accel back to normal
G1 X0 Y0 Z15 F5000           ; get out the way
M117 lets get digital...         ; LCD status message
M400                         ; clear buffer
M121
G1 Z-5
G4 S6                        ; pause

G1 X0 Y0 F4500
G4 S4
G1 X67.5 Y0 F4500
G4 S4
G1 X135 Y0 F4500
G4 S4
G1 X202.5 Y0 F4500
G4 S4
G1 X270 Y0 F4500
G4 S4

G1 X0 Y67.5 F8500
G4 S4
G1 X67.5 Y67.5 F4500
G4 S4
G1 X135 Y67.5 F4500
G4 S4
G1 X202.5 Y67.5 F4500
G4 S4
G1 X270 Y67.5 F4500
G4 S4

G1 X0 Y135 F8500
G4 S4
G1 X67.5 Y135 F4500
G4 S4
G1 X135 Y135 F4500
G4 S4
G1 X202.5 Y135 F4500
G4 S4
G1 X270 Y135 F4500
G4 S4

G1 X0 Y202.5 F8500
G4 S4
G1 X67.5 Y202.5 F4500
G4 S4
G1 X135 Y202.5 F4500
G4 S4
G1 X202.5 Y202.5 F4500
G4 S4
G1 X270 Y202.5 F4500
G4 S4

G1 X0 Y270 F8500
G4 S4
G1 X67.5 Y270 F4500
G4 S4
G1 X135 Y270 F4500
G4 S4
G1 X202.5 Y270 F4500
G4 S4
G1 X270 Y270 F4500
G4 S4

G1 X0 Y0 
G4 S4
G1 Z5

